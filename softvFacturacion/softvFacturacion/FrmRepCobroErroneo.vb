﻿Imports System.Data.SqlClient
Imports System.Collections.Generic

Public Class FrmRepCobroErroneo

    Private Sub FrmRepCobroErroneo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dtpFechaIni.Value = Today
        dtpFechaFin.Value = Today

    End Sub

    Private Sub FechaIni_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaIni.ValueChanged
        dtpFechaFin.MinDate = dtpFechaIni.Value
    End Sub

    Private Sub FechaFin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaFin.ValueChanged
        dtpFechaIni.MaxDate = dtpFechaFin.Value
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        eFechaIni = dtpFechaIni.Value
        eFechaFin = dtpFechaFin.Value
        GloReporte = 11
        FrmImprimirRepGral.Show()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

End Class