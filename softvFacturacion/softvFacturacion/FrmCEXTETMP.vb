Imports System.Data.SqlClient
Public Class FrmCEXTETMP

    Private Sub NUECEXTETMP(ByVal Clv_Session As Long, ByVal Contrato As Long, ByVal ExtAdic As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NUECEXTETMP", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Contrato
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@ExtAdic", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = ExtAdic
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub CONCONEXBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCONEXBindingNavigatorSaveItem.Click
        If IsNumeric(Me.ExtAdicTextBox.Text) = False Then
            MsgBox("Teclea el n�mero de extensiones.", MsgBoxStyle.Information)
            Me.ExtAdicTextBox.Clear()
            Exit Sub
        End If
        If CInt(Me.ExtAdicTextBox.Text) = 0 Then
            MsgBox("Teclea un n�mero de extensiones v�lido.", MsgBoxStyle.Information)
            Exit Sub
        End If
        NUECEXTETMP(GloTelClv_Session, GloContrato, CInt(Me.ExtAdicTextBox.Text))
        GloCEXTTE = True
        GloBndExt = True
        GloClv_Txt = "CEXTV"
        Me.Close()
    End Sub

    Private Sub FrmCEXTETMP_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me)
        Me.ExtAdicLabel.ForeColor = Color.Black
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class