<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmBonificacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown
        Me.CMBLabel1 = New System.Windows.Forms.Label
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.CMBLabel2 = New System.Windows.Forms.Label
        Me.CMBLabel3 = New System.Windows.Forms.Label
        Me.REDLabel4 = New System.Windows.Forms.Label
        Me.REDLabel5 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.cmbLabel100 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.DataSetEdgar = New softvFacturacion.DataSetEdgar
        Me.DAMEUTLIMODIABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMEUTLIMODIATableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.DAMEUTLIMODIATableAdapter
        Me.DameFilaPreDetFacturas_2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameFilaPreDetFacturas_2TableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.DameFilaPreDetFacturas_2TableAdapter
        Me.ModFilaPreDetFacturasBonificacionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ModFilaPreDetFacturasBonificacionTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.ModFilaPreDetFacturasBonificacionTableAdapter
        Me.InsPreMotivoBonificacionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsPreMotivoBonificacionTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.InsPreMotivoBonificacionTableAdapter
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMEUTLIMODIABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameFilaPreDetFacturas_2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModFilaPreDetFacturasBonificacionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsPreMotivoBonificacionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumericUpDown1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown1.Location = New System.Drawing.Point(250, 137)
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(82, 24)
        Me.NumericUpDown1.TabIndex = 0
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(18, 139)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(226, 18)
        Me.CMBLabel1.TabIndex = 1
        Me.CMBLabel1.Text = "Numero de Días a Bonificar :"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(379, 269)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(123, 43)
        Me.Button2.TabIndex = 51
        Me.Button2.Text = "&CANCELAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(238, 269)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(124, 43)
        Me.Button1.TabIndex = 50
        Me.Button1.Text = "&ACEPTAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(250, 100)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(112, 22)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.TabStop = False
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(168, 100)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(75, 18)
        Me.CMBLabel2.TabIndex = 9
        Me.CMBLabel2.Text = "Importe :"
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(72, 24)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(165, 18)
        Me.CMBLabel3.TabIndex = 10
        Me.CMBLabel3.Text = "Servicio a Bonificar :"
        '
        'REDLabel4
        '
        Me.REDLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.REDLabel4.ForeColor = System.Drawing.Color.Red
        Me.REDLabel4.Location = New System.Drawing.Point(247, 24)
        Me.REDLabel4.Name = "REDLabel4"
        Me.REDLabel4.Size = New System.Drawing.Size(305, 23)
        Me.REDLabel4.TabIndex = 11
        Me.REDLabel4.Text = "Label4"
        '
        'REDLabel5
        '
        Me.REDLabel5.AutoSize = True
        Me.REDLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.REDLabel5.ForeColor = System.Drawing.Color.Red
        Me.REDLabel5.Location = New System.Drawing.Point(83, 180)
        Me.REDLabel5.Name = "REDLabel5"
        Me.REDLabel5.Size = New System.Drawing.Size(160, 18)
        Me.REDLabel5.TabIndex = 12
        Me.REDLabel5.Text = "Importe Bonificado :"
        '
        'TextBox2
        '
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(251, 180)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(111, 22)
        Me.TextBox2.TabIndex = 2
        Me.TextBox2.TabStop = False
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(3, 24)
        Me.TextBox3.Multiline = True
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(335, 161)
        Me.TextBox3.TabIndex = 1
        '
        'cmbLabel100
        '
        Me.cmbLabel100.AutoSize = True
        Me.cmbLabel100.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbLabel100.Location = New System.Drawing.Point(3, 3)
        Me.cmbLabel100.Name = "cmbLabel100"
        Me.cmbLabel100.Size = New System.Drawing.Size(203, 18)
        Me.cmbLabel100.TabIndex = 15
        Me.cmbLabel100.Text = "Motivo de la Bonificación:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.TextBox3)
        Me.Panel1.Controls.Add(Me.cmbLabel100)
        Me.Panel1.Location = New System.Drawing.Point(379, 60)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(341, 188)
        Me.Panel1.TabIndex = 16
        '
        'DataSetEdgar
        '
        Me.DataSetEdgar.DataSetName = "DataSetEdgar"
        Me.DataSetEdgar.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DAMEUTLIMODIABindingSource
        '
        Me.DAMEUTLIMODIABindingSource.DataMember = "DAMEUTLIMODIA"
        Me.DAMEUTLIMODIABindingSource.DataSource = Me.DataSetEdgar
        '
        'DAMEUTLIMODIATableAdapter
        '
        Me.DAMEUTLIMODIATableAdapter.ClearBeforeFill = True
        '
        'DameFilaPreDetFacturas_2BindingSource
        '
        Me.DameFilaPreDetFacturas_2BindingSource.DataMember = "DameFilaPreDetFacturas_2"
        Me.DameFilaPreDetFacturas_2BindingSource.DataSource = Me.DataSetEdgar
        '
        'DameFilaPreDetFacturas_2TableAdapter
        '
        Me.DameFilaPreDetFacturas_2TableAdapter.ClearBeforeFill = True
        '
        'ModFilaPreDetFacturasBonificacionBindingSource
        '
        Me.ModFilaPreDetFacturasBonificacionBindingSource.DataMember = "ModFilaPreDetFacturasBonificacion"
        Me.ModFilaPreDetFacturasBonificacionBindingSource.DataSource = Me.DataSetEdgar
        '
        'ModFilaPreDetFacturasBonificacionTableAdapter
        '
        Me.ModFilaPreDetFacturasBonificacionTableAdapter.ClearBeforeFill = True
        '
        'InsPreMotivoBonificacionBindingSource
        '
        Me.InsPreMotivoBonificacionBindingSource.DataMember = "InsPreMotivoBonificacion"
        Me.InsPreMotivoBonificacionBindingSource.DataSource = Me.DataSetEdgar
        '
        'InsPreMotivoBonificacionTableAdapter
        '
        Me.InsPreMotivoBonificacionTableAdapter.ClearBeforeFill = True
        '
        'FrmBonificacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(740, 352)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.REDLabel5)
        Me.Controls.Add(Me.REDLabel4)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.NumericUpDown1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "FrmBonificacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Bonificaciones"
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMEUTLIMODIABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameFilaPreDetFacturas_2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModFilaPreDetFacturasBonificacionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsPreMotivoBonificacionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DataSetEdgar As softvFacturacion.DataSetEdgar
    Friend WithEvents DAMEUTLIMODIABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMEUTLIMODIATableAdapter As softvFacturacion.DataSetEdgarTableAdapters.DAMEUTLIMODIATableAdapter
    Friend WithEvents DameFilaPreDetFacturas_2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameFilaPreDetFacturas_2TableAdapter As softvFacturacion.DataSetEdgarTableAdapters.DameFilaPreDetFacturas_2TableAdapter
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents REDLabel4 As System.Windows.Forms.Label
    Friend WithEvents REDLabel5 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents ModFilaPreDetFacturasBonificacionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ModFilaPreDetFacturasBonificacionTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.ModFilaPreDetFacturasBonificacionTableAdapter
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents cmbLabel100 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents InsPreMotivoBonificacionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsPreMotivoBonificacionTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.InsPreMotivoBonificacionTableAdapter
End Class
