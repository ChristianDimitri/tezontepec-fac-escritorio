Imports System.Data.SqlClient
Imports System.Text

Public Class FrmSelCliente

    'Private Sub BUSCACLIENTES(ByVal OP As Integer)
    '    Try
    '        Dim CON As New SqlConnection(MiConexion)
    '        CON.Open()
    '        If OP = 0 Then
    '            If IsNumeric(Me.bcONTRATO.Text) = True Then
    '                Me.BUSCLIPORCONTRATO2TableAdapter.Connection = CON
    '                Me.BUSCLIPORCONTRATO2TableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO2, New System.Nullable(Of Long)(CType(bcONTRATO.Text, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)))
    '            Else
    '                Me.BUSCLIPORCONTRATO2TableAdapter.Connection = CON
    '                Me.BUSCLIPORCONTRATO2TableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO2, New System.Nullable(Of Long)(CType(0, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)))
    '            End If
    '            Me.bcONTRATO.Clear()
    '        ElseIf OP = 1 Then
    '            Me.BUSCLIPORCONTRATO2TableAdapter.Connection = CON
    '            Me.BUSCLIPORCONTRATO2TableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO2, New System.Nullable(Of Long)(CType(0, Long)), (CType(Me.BNOMBRE.Text, String)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)))
    '            'Me.BNOMBRE.Clear()
    '        ElseIf OP = 2 Then
    '            Me.BUSCLIPORCONTRATO2TableAdapter.Connection = CON
    '            Me.BUSCLIPORCONTRATO2TableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO2, New System.Nullable(Of Long)(CType(0, Long)), (CType("", String)), (CType(Me.BCALLE.Text, String)), (CType(Me.BNUMERO.Text, String)), (CType(Me.BCIUDAD.Text, String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)))
    '            'Me.BCALLE.Clear()
    '            'Me.BNUMERO.Clear()
    '            'Me.BCIUDAD.Clear()
    '        Else
    '            Me.BUSCLIPORCONTRATO2TableAdapter.Connection = CON
    '            Me.BUSCLIPORCONTRATO2TableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO2, New System.Nullable(Of Long)(CType(0, Long)), "", "", "", "", (CType("", String)), New System.Nullable(Of Integer)(CType(3, Integer)))
    '            'Me.BCALLE.Clear()
    '            'Me.BNUMERO.Clear()
    '            'Me.BCIUDAD.Clear()
    '        End If
    '        CON.Close()
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub

    Private Sub BUSCACLIENTES(ByVal OP As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If OP = 0 Then
                If IsNumeric(Me.bcONTRATO.Text) = True Then
                    'Me.BUSCLIPORCONTRATO2TableAdapter.Connection = CON
                    'Me.BUSCLIPORCONTRATO2TableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO2, New System.Nullable(Of Long)(CType(bcONTRATO.Text, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)))
                    BusCliPorContratoSeparado(bcONTRATO.Text, "", "", "", "", "", "", "", OP, Me.cmbColonias.SelectedValue, 0)
                Else

                    MsgBox("La Busqueda no se puede realizar con datos invalidos", MsgBoxStyle.Information)
                End If
                Me.bcONTRATO.Clear()
            End If
            If OP = 1 Then
                If Len(Trim(Me.BNOMBRE.Text)) > 0 Or Len(Trim(Me.APaternoTextBox.Text)) > 0 Or Len(Trim(Me.AMaternoTextBox.Text)) > 0 Then

                    BusCliPorContratoSeparado(0, Me.BNOMBRE.Text, Me.APaternoTextBox.Text, Me.AMaternoTextBox.Text, "", "", "", "", OP, Me.cmbColonias.SelectedValue, 0)
                Else
                    MsgBox("La Busqueda no se puede realizar con datos invalidos", MsgBoxStyle.Information)

                End If
            End If

            'Me.BNOMBRE.Clear()
            If OP = 2 Then
                If Len(Trim(Me.BCALLE.Text)) > 0 Or Len(Trim(Me.BNUMERO.Text)) > 0 Or Len(Trim(Me.BCIUDAD.Text)) > 0 Or CInt(Me.cmbColonias.SelectedValue) > 0 Then
                    BusCliPorContratoSeparado(0, "", "", "", Me.BCALLE.Text, Me.BNUMERO.Text, Me.BCIUDAD.Text, "", OP, Me.cmbColonias.SelectedValue, 0)
                Else
                    MsgBox("La Busqueda no se puede realizar con datos invalidos", MsgBoxStyle.Information)
                End If
            End If
            If OP = 3 Then
                BusCliPorContratoSeparado(0, "", "", "", "", "", "", "", OP, Me.cmbColonias.SelectedValue, 0)
            End If
            If OP = 4 Then
                If IsNumeric(Me.BCPlaca.Text) = True Then
                    BusCliPorContratoSeparado(0, "", "", "", "", "", "", "", OP, Me.cmbColonias.SelectedValue, Me.BCPlaca.Text)
                Else
                    MsgBox("La busquedad no se puede realizar con datos invalidos.", MsgBoxStyle.Information)
                End If

            End If

            ' BusCliPorContratoSeparado(0, "", "", "", "", "", "", "", OP)

            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub BusCliPorContratoSeparado(ByVal ContratoCli As Long, ByVal NombreCli As String, ByVal APaternoCli As String, ByVal AMaternoCli As String, _
                                 ByVal CalleCli As String, ByVal NumeroCli As String, ByVal CiudadCli As String, ByVal Telefono As String, ByVal OpCli As Integer, ByVal prmClvColonia As Integer, ByVal placa As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC uspBusCliPorContratoSeparado ")
        StrSQL.Append(CStr(ContratoCli) & ",")
        StrSQL.Append("'" & NombreCli & "',")
        StrSQL.Append("'" & APaternoCli & "',")
        StrSQL.Append("'" & AMaternoCli & "',")
        StrSQL.Append("'" & CalleCli & "',")
        StrSQL.Append("'" & NumeroCli & "',")
        StrSQL.Append("'" & CiudadCli & "',")
        StrSQL.Append("'" & Telefono & "',")
        StrSQL.Append(CStr(OpCli) & ",")
        StrSQL.Append(CStr(prmClvColonia) & ",")
        StrSQL.Append(CStr(placa))


        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.DataGridView1.DataSource = BS.DataSource
            Try
                Me.CONTRATOLabel1.Text = CStr(Me.DataGridView1.SelectedCells(0).Value)
                Me.NOMBRELabel1.Text = (Me.DataGridView1.SelectedCells(1).Value + " " + Me.DataGridView1.SelectedCells(2).Value + " " + Me.DataGridView1.SelectedCells(3).Value + " ")
                Me.CALLELabel1.Text = (Me.DataGridView1.SelectedCells(4).Value)
                Me.NUMEROLabel1.Text = (Me.DataGridView1.SelectedCells(5).Value)
                Me.COLONIALabel1.Text = (Me.DataGridView1.SelectedCells(6).Value)
            Catch ex As System.Exception
                'System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
            Glocontratosel = Me.CONTRATOLabel1.Text
            If bnd1conciliacion = True Then
                bnd1conciliacion = False
                bnd2conciliacion = True
            End If

            Me.Close()
        End If

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.BUSCACLIENTES(0)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.BUSCACLIENTES(1)
    End Sub




    Private Sub BrwClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If IdSistema = "VA" Or IdSistema = "LO" Then
            FrmSelCliente2.Show()
            Me.Close()
            Exit Sub
        End If
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUESTRACALLES' Puede moverla o quitarla seg�n sea necesario.
        Me.MUESTRACALLESTableAdapter.Connection = CON
        Me.MUESTRACALLESTableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRACALLES)
        CON.Close()
        llenaComboColonias()
        Me.BUSCACLIENTES(3)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub bcONTRATO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles bcONTRATO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(0)
        End If
    End Sub



    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Me.BUSCACLIENTES(2)
    End Sub

    Private Sub BNOMBRE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(1)
            'Me.BNOMBRE.Text = ""
        End If
    End Sub



    Private Sub BCALLE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCALLE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub



    Private Sub BNUMERO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNUMERO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub


    Private Sub BCIUDAD_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCIUDAD.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub


    Private Sub CONTRATOLabel1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CONTRATOLabel1.TextChanged
        Try
            CREAARBOL()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub SOLOINTERNETCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SOLOINTERNETCheckBox.CheckedChanged
        If Me.SOLOINTERNETCheckBox.Checked = False Then
            Me.SOLOINTERNETCheckBox.Enabled = False
        Else
            Me.SOLOINTERNETCheckBox.Enabled = True
        End If
    End Sub

    Private Sub ESHOTELCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ESHOTELCheckBox.CheckedChanged
        If Me.ESHOTELCheckBox.Checked = False Then
            Me.ESHOTELCheckBox.Enabled = False
        Else
            Me.ESHOTELCheckBox.Enabled = True
        End If
    End Sub

    Private Sub ServicioListBox_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Me.bcONTRATO.Focus()
    End Sub




    Private Sub CREAARBOL()

        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next

            If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.DameSerDELCliFACTableAdapter.Connection = CON
                Me.DameSerDELCliFACTableAdapter.Fill(Me.NewsoftvDataSet.DameSerDELCliFAC, New System.Nullable(Of Long)(CType(Me.CONTRATOLabel1.Text, Long)))
                CON.Close()
            End If
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewsoftvDataSet.DameSerDELCliFAC.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 16) = "Servicio De Tele" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisi�n Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Tel�fonia" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                            pasa = False
                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next
            Me.TreeView1.Nodes(0).ExpandAll()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub



    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
            Glocontratosel = Me.CONTRATOLabel1.Text
            If bnd1conciliacion = True Then
                bnd1conciliacion = False
                bnd2conciliacion = True
            End If
            Me.Close()
        End If
    End Sub




    Private Sub Button7_Click_1(sender As System.Object, e As System.EventArgs) Handles Button7.Click
        BUSCACLIENTES(1)
    End Sub

    Private Sub DataGridView1_CurrentCellChanged(sender As System.Object, e As System.EventArgs) Handles DataGridView1.CurrentCellChanged
        Try
            Me.CONTRATOLabel1.Text = CStr(Me.DataGridView1.SelectedCells(0).Value)
            Me.NOMBRELabel1.Text = (Me.DataGridView1.SelectedCells(1).Value + " " + Me.DataGridView1.SelectedCells(2).Value + " " + Me.DataGridView1.SelectedCells(3).Value + " ")
            Me.CALLELabel1.Text = (Me.DataGridView1.SelectedCells(4).Value)
            Me.NUMEROLabel1.Text = (Me.DataGridView1.SelectedCells(5).Value)
            Me.COLONIALabel1.Text = (Me.DataGridView1.SelectedCells(6).Value)
        Catch ex As System.Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BNOMBRE_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles BNOMBRE.KeyDown, APaternoTextBox.KeyDown, AMaternoTextBox.KeyDown
        If e.KeyCode = Keys.Enter Then
            Button7_Click_1(sender, e)
        End If
    End Sub

#Region "COMBO COLONIAS"
    Private Sub llenaComboColonias()
        Try
            BaseII.limpiaParametros()
            Me.cmbColonias.DataSource = BaseII.ConsultaDT("uspConsultaColonias")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        BUSCACLIENTES(4)
    End Sub
End Class