﻿Public Class FrmGastos

    Public prmOpcion As String
    Public prmClaveGasto As Integer
    Public prmImporteAnterior As Decimal

    Private Sub FrmGastos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)

        '''''SE LLENA EL COMBO DE LOS TIPOS DE GASTOS (INICIO)
        ControlEfectivoClass.limpiaParametros()
        Me.cmbTipoGasto.DisplayMember = "descripcion"
        Me.cmbTipoGasto.ValueMember = "valor"
        Me.cmbTipoGasto.DataSource = ControlEfectivoClass.ConsultaDT("uspLlenaComboTipoGasto")
        '''''SE LLENA EL COMBO DE LOS TIPOS DE GASTOS (FIN)

        If prmOpcion = "N" Then
            '''''SE ESTABLECE COMO VALOR PRINCIPAL DEL DTP EL DÍA QUE SE ESTÉ INGRESANDO CUANDO SEA UN NUEVO REGISTRO (INICIO)
            Me.dtpFecha.Value = Today
            Me.dtpFecha.Enabled = False
            Me.txtUsuario.Text = GloUsuario
            '''''SE ESTABLECE COMO VALOR PRINCIPAL DEL DTP EL DÍA QUE SE ESTÉ INGRESANDO CUANDO SEA UN NUEVO REGISTRO (FIN)
        End If

        '''''SE HABILITA EL CAMPO DE LA FECHA PARA CONTROL TOTA Y ENCARGADO DE SUCURSAL (INICIO)
        If GloTipoUsuario = 40 Or GloTipoUsuario = 1 Then
            Me.dtpFecha.Enabled = True
        Else
            Me.dtpFecha.Enabled = False
        End If
        '''''SE HABILITA EL CAMPO DE LA FECHA PARA CONTROL TOTA Y ENCARGADO DE SUCURSAL (FIN)

        If prmOpcion = "C" Then '''''SE BLOQUEAN LOS CAMPOS PARA CUANDO SÓLO SE VA A CONSULTAR
            Me.txtDescripcion.ReadOnly = True
            Me.txtImporte.ReadOnly = True
            Me.cmbTipoGasto.Enabled = False
            Me.btnAceptar.Enabled = False
            Me.dtpFecha.Enabled = False
        Else '''''EN CASO CONTRARIO QUE NO SEA CONSULTA SE HABILITAN LOS CAMPOS
            Me.txtDescripcion.ReadOnly = False
            Me.txtImporte.ReadOnly = False
            Me.cmbTipoGasto.Enabled = True
            Me.btnAceptar.Enabled = True
        End If

        If prmOpcion = "M" Or prmOpcion = "C" Then '''''SE OBTIENEN LOS DATOS DEL CLIENTE CUANDO ES UNA CONSULTA O MODIFICACIÓN
            LlenaCampos(prmClaveGasto, 0, "T", "TODOS", "", GloUsuario)
        End If

        '''''CALCULAMOS EL EFECTIVO DISPONIBLE DE LA CAJERA (INICIO)
        uspCalculaEfectivo(GloUsuario, Me.dtpFecha.Value)
        '''''CALCULAMOS EL EFECTIVO DISPONIBLE DE LA CAJERA (FIN)
    End Sub

    Private Sub LlenaCampos(ByVal prmIdGasto As Long, ByVal prmIdTipoGasto As Long, ByVal prmStatus As String, ByVal prmCajera As String, ByVal prmDescripcion As String, ByVal prmLoginUsuario As String)
        '''''LLENAMOS LOS CAMPOS CON LA INFORMACIÓN CORRESPONDIENTE (INICIO)
        Dim DT As New DataTable
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@idGasto", SqlDbType.BigInt, prmIdGasto)
        ControlEfectivoClass.CreateMyParameter("@idTipoGasto", SqlDbType.BigInt, prmIdTipoGasto)
        ControlEfectivoClass.CreateMyParameter("@status", SqlDbType.VarChar, prmStatus, 1)
        ControlEfectivoClass.CreateMyParameter("@cajera", SqlDbType.VarChar, prmCajera, 5)
        ControlEfectivoClass.CreateMyParameter("@descripcion", SqlDbType.VarChar, prmDescripcion, 250)
        ControlEfectivoClass.CreateMyParameter("@loginUsuario", SqlDbType.VarChar, prmLoginUsuario, 5)

        DT = ControlEfectivoClass.ConsultaDT("uspConsultaGastos")
        Me.txtClaveGasto.Text = DT.Rows(0)("idGasto").ToString
        Me.dtpFecha.Value = DT.Rows(0)("fecha").ToString
        Me.txtUsuario.Text = DT.Rows(0)("clvUsuario").ToString
        Me.cmbTipoGasto.SelectedValue = CInt(DT.Rows(0)("idTipoGasto").ToString)
        Me.txtDescripcion.Text = DT.Rows(0)("descripcion").ToString
        Me.txtImporte.Text = CDec(DT.Rows(0)("importe")).ToString("#.00")
        '''''LLENAMOS LOS CAMPOS CON LA INFORMACIÓN CORRESPONDIENTE (FIN)
    End Sub

    '''''MÉTODO QUE EJECUTA EL PROCEDIMIENTO DE INSERCIÓN DE LOS DATOS A LA TABLA DE LOS GASTOS
    Private Sub INSERTAR(ByVal prmIdTipoGasto As Long, ByVal prmCajera As String, ByVal prmDescripcion As String, ByVal prmImporte As Decimal)
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@idTipoGasto", SqlDbType.BigInt, prmIdTipoGasto)
        ControlEfectivoClass.CreateMyParameter("@clvUsuario", SqlDbType.VarChar, prmCajera, 5)
        ControlEfectivoClass.CreateMyParameter("@descripcion", SqlDbType.VarChar, prmDescripcion, 250)
        ControlEfectivoClass.CreateMyParameter("@importe", SqlDbType.Money, prmImporte)

        ControlEfectivoClass.Inserta("uspInsertaGastos")
        MsgBox("Registro Almacenado Exitosamente", MsgBoxStyle.Information)
    End Sub

    '''''MÉTODO QUE EJECUTA EL PROCEDIMIENTO DE MODIFICACIÓN DE LOS DATOS A LA TABLA DE LOS GASTOS
    Private Sub MODIFICAR(ByVal prmIdGasto As Long, ByVal prmIdTipoGasto As Long, ByVal prmDescripcion As String, ByVal prmImporte As Decimal, ByVal prmOp As Integer)
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@idGasto", SqlDbType.BigInt, prmIdGasto)
        ControlEfectivoClass.CreateMyParameter("@idTipoGasto", SqlDbType.BigInt, prmIdTipoGasto)
        ControlEfectivoClass.CreateMyParameter("@descripcion", SqlDbType.VarChar, prmDescripcion, 250)
        ControlEfectivoClass.CreateMyParameter("@importe", SqlDbType.Money, prmImporte)
        ControlEfectivoClass.CreateMyParameter("@op", SqlDbType.Int, prmOp)

        ControlEfectivoClass.Inserta("uspModificaGastos")
        MsgBox("Registro Almacenado Exitosamente", MsgBoxStyle.Information)
    End Sub

    Private Sub uspCalculaEfectivo(ByVal prmCajera As String, ByVal prmFecha As Date)
        '''''EJECUTAMOS EL PROCEDIMIENTO QUE NOS VA A DAR LOS TOTALES DEL EFECTIVO EN EL DÍA POR LA CAJERA (INICIO)
        Dim DT As New DataTable

        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@cajera", SqlDbType.VarChar, prmCajera, 5)
        ControlEfectivoClass.CreateMyParameter("@fecha", SqlDbType.DateTime, prmFecha)

        DT = ControlEfectivoClass.ConsultaDT("uspCalculaEfectivo")
        Me.txtEfectivoCobrado.Text = CDec(DT.Rows(0)("totalEfectivo")).ToString("#.00")
        '''''EJECUTAMOS EL PROCEDIMIENTO QUE NOS VA A DAR LOS TOTALES DEL EFECTIVO EN EL DÍA POR LA CAJERA (FIN)
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If Me.cmbTipoGasto.Items.Count = 0 Then '''''SE VALIDA QUE HAYA SELECCIONADO AL MENOS UN TIPO DE GASTO
            If IsNumeric(Me.cmbTipoGasto.SelectedValue) = True Then
                MsgBox("Capture un Tipo de Gasto para continuar", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If

        If Me.txtDescripcion.Text.Length = 0 Then '''''SE VALIDA QUE HAYAN INGRESADO UNA DESCRIPCIÓN AL GASTO
            MsgBox("Capture una Descripción para continuar", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.txtImporte.Text.Length = 0 Then '''''SE VALIDA QUE SE HAYA INGRESADO EL IMPORTE DEL GASTO
            MsgBox("Capture el Importe del Gasto para continuar", MsgBoxStyle.Information)
            Exit Sub
        End If

        If IsNumeric(Me.txtImporte.Text) = False Then
            MsgBox("Capture un Importe Válido para continuar", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.cmbTipoGasto.SelectedValue = 0 Then
            MsgBox("Capture un Tipo de Gasto para continuar", MsgBoxStyle.Information)
            Exit Sub
        End If

        If CDec(Me.txtImporte.Text) > CDec(Me.txtEfectivoCobrado.Text) Then
            MsgBox("El Importe no puede ser mayor al Efectivo Disponible", MsgBoxStyle.Information)
            Exit Sub
        End If

        If prmOpcion = "N" Then ''''SE VALIDA QUE CUANDO SEA NUEVO SE MANDE EL MÉTODO DE INSERTAR
            INSERTAR(Me.cmbTipoGasto.SelectedValue, GloUsuario, Me.txtDescripcion.Text, Me.txtImporte.Text)
            bitsist(GloUsuario, 0, "Facturacion ", Me.Name, "", "Se Registra Gasto por: " + CStr(Me.txtImporte.Text), "", SubCiudad)

        End If

        If prmOpcion = "M" Then ''''SE VALIDA QUE CUANDO SEA MODIFICAR SE MANDE EL MÉTODO DE MODIFICAR
            MODIFICAR(prmClaveGasto, Me.cmbTipoGasto.SelectedValue, Me.txtDescripcion.Text, Me.txtImporte.Text, 0)
            bitsist(GloUsuario, 0, "Facturacion ", Me.Name, "", "Se Modifica Gasto # " + CStr(prmClaveGasto) + " por $" + CStr(Me.txtImporte.Text) + " de: " + CStr(Me.txtUsuario.Text), "Importe Anterior" + CStr(prmImporteAnterior), SubCiudad)
        End If

        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class