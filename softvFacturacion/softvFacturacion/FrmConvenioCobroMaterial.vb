﻿Imports System.Text
Imports System.Xml
Imports System.IO

Public Class FrmConvenioCobroMaterial

    Public idConvenio As Long
    Public idBitacora As Long

    Private Sub FrmConvenioCobroMaterial_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim DT As DataTable = CobroParcialMaterial.spCalculaConvenioCobroMaterial(idBitacora)

        Me.txtContrato.Text = DT.Rows(0)("CONTRATO").ToString
        Me.txtImporte.Text = DT.Rows(0)("SUMA").ToString
        Me.txtNumPagos.Text = DT.Rows(0)("NumMaxPagos").ToString
        Me.txtImporteMensual.Text = DT.Rows(0)("ImporteXmes").ToString
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub btnRechazarConvenio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRechazarConvenio.Click
        Me.Close()
    End Sub

    Private Sub btnAceptarConvenio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptarConvenio.Click
        CobroParcialMaterial.spInsertaParcialidadesCobroMaterial(Me.txtContrato.Text, idBitacora, Me.txtImporte.Text, Me.txtNumPagos.Text, Me.txtImporteMensual.Text)
        bnd2pardep1 = True
        Me.Close()
    End Sub
End Class