Imports System.Data.SqlClient
Imports System.Text
Public Class FrmMotCan

    Private Sub MuestraMotCan(ByVal Op As Integer, ByVal Clv_MotCan As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraMotCan ")
        strSQL.Append(CStr(Op) & ", ")
        strSQL.Append(CStr(Clv_MotCan))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource


        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBox1.DataSource = bindingSource
            eClvMotCan = Me.ComboBox1.SelectedValue
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eClvMotCan = Me.ComboBox1.SelectedValue
        GuardaMontCance(eClvMotCan)
        FrmFAC.CobroDeAdeudo()
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        eClvMotCan = 0
        Me.Close()
    End Sub

    Private Sub FrmMotCan_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me)
        MuestraMotCan(0, 0)
    End Sub
    Private Sub GuardaMontCance(ByVal MotCan As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("RelMotCancelacionTem", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@MotCan", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = MotCan
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_session", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = gloClv_Session
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            conexion.Dispose()
           
        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub
End Class